/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fedex.tracking;
/*import static fedex.tracking.FedEXTracking.DB_URL;
import static fedex.tracking.FedEXTracking.PASS;
import static fedex.tracking.FedEXTracking.USER;
*/
import java.sql.*;
import java.applet.*;
import java.util.*;
import java.text.DateFormat;
import java.util.Timer;

/**
 *
 * @author Karthik
 */
public class SqlManager{
    
       
    static final String DB_URL = "jdbc:mysql://localhost:3306/test";
    static final String USER = "root";
    static final String PASS = "";
   
    public static Connection conn = null;
    public static Statement stmt;
    
    
   
    public static void showFullTable(ResultSet rs) throws SQLException, InterruptedException
    {
        /*
        Prints out attributes of the chosen TrackingId
        Param:sql.Resultset
        Return:None
        */
        Package box = new Package();
        int distance = 0;
        System.out.println("ID: " + rs.getInt("TrackingId") +"\t\t\t\t Service Type:" + box.serviceType+ "\n");
        System.out.println("Weight of Package: " + rs.getDouble("Weight_Kg")+"kg/"+rs.getDouble("Weight_Lbs")+"lbs"+ "\t Package: " + box.packaging+"\n");
        
            System.out.println("TotalPieces: " + rs.getInt("TotalPieces") + "\t\t\t\t"
                + " Signature Services: "+ box.signatureServices+"\n");
        System.out.println("StartCity:"+ rs.getString("StoreHouse")+ "\t\t\t Destination:" + rs.getString("Destination")+"\n");
        System.out.println("Dimensions: " + rs.getString("Dimensions") +"\n");
        System.out.println("CURRENT STATUS:"+ rs.getString("Status"));
        //box.path = Dijkstra.getPath(rs.getString("StartCity"), rs.getString("Destination"));
        
       // System.out.println(box.path +"\n");
    }
        
    public static void insertInTable(Connection conn, Statement stmt, Double Weight_kgs,
            Double Weight_Lbs, String Dimensions, int Total_pieces, String startCity, String destination) throws SQLException, InterruptedException
    {
        /*
        Method allows insertion to Table "Attribute"
        Param: sql.Statement, sql.Connection, int TrackingId,
               Double wKgs, Double wLbs, String Dimensions, int TotalPieces
        return: void
        */
        
        //Connection connect = DriverManager.getConnection(DB_URL, USER, PASS);
        //Statement statement = connect.createStatement();
        //conn = DriverManager.getConnection(DB_URL, USER, PASS);
        //stmt = conn.createStatement();
            
        try
        {
            String sql = "INSERT INTO packages(Weight_Kg, Weight_Lbs, Dimensions, TotalPieces, StoreHouse, Destination) "+"VALUES(" 
              + Weight_kgs + ", " + Weight_Lbs +",'"
              +Dimensions+"', "+ Total_pieces
              +",'"+ startCity+"','"+destination+"')";
        stmt.executeUpdate(sql);
        //Thread.sleep(1000);
        }
        catch(SQLException e){
        }
        /*finally{
            conn.close();
            stmt.close();
      }*/
    }
    
    
    public static void establishConnection() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException
    {
      /* 
        Method responsible for establishing JDBC Connection
        Param: None
        Return: None
        */ 
      Class.forName("com.mysql.jdbc.Driver").newInstance();
      //Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
      System.out.println("Connected database successfully...");
      System.out.println("*******************************************************************");
      //conn.close();
    }
    
    public static void retrieveFromTable(Statement stmt, 
            Connection connect, String TrackingId) throws SQLException, InterruptedException
    {
        /*
        Retrieves the attributes based on the TrackingId fed in
        Param: sql.Statement, sql.Connection, String TrackingId
        Return:None
        */
        String query = "SELECT * FROM packages WHERE TrackingId = '" + 
                TrackingId +"'";
        try (ResultSet rs = stmt.executeQuery(query)) {
            while(rs.next()){
                SqlManager.showFullTable(rs);}
        }        
        stmt.close();
        connect.close();
    }
    public static void retrieveAll(Statement stmt,Connection conn) throws SQLException, InterruptedException
    {
        /*
        Displays attributes of all packages in database
        Param: sql.Statement, sql.Connection
        Return: None
        */
        String query = "SELECT * FROM packages";
        try (ResultSet result = stmt.executeQuery(query)) {
            while(result.next()){
                SqlManager.showFullTable(result);
            }
            result.close();
            stmt.close();
            conn.close();
        }
        
    }
    
}
