/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fedex.tracking;


import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Karthik
 */
public class FedEXTracking {

    /**
     * @param args the command line arguments
     */  

    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        Statement stmt = null;
                
   try{
      SqlManager.establishConnection();
      conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");
      stmt = conn.createStatement();
      Package box = new Package();
     
      /*for(int i = 1; i < 10000; i++)
      {
          Double wKg = box.weightRandomizer();
          Double wlb = box.lbsConverter();
          String startCity = box.startCityRandomizer();
          String destination = box.destinationRandomizer(startCity);
          SqlManager.insertInTable(conn, stmt,wKg, wlb, box.dimensionsRandomizer(),box.fetchTotalPieces(), startCity, destination);
      }
    //PackageThread thread1 = new PackageThread();
    //thread1.start();
    /*PackageThread thread2 = new PackageThread();
    thread2.start();
    PackageThread thread3 = new PackageThread();
    thread3.start();
    PackageThread thread4 = new PackageThread();
    thread4.start();
    PackageThread thread5 = new PackageThread();
    thread5.start();
    PackageThread thread6 = new PackageThread();
    thread6.start();
    PackageThread thread7 = new PackageThread();
    thread7.start();
    PackageThread thread8 = new PackageThread();
    thread8.start();
    PackageThread thread9 = new PackageThread();
    thread9.start();
    PackageThread thread10 = new PackageThread();
    thread10.start();
      */
    UpdateThread thread11 = new UpdateThread();
      thread11.start();
      
    QueryThread thread12 = new QueryThread();
      thread12.start();
    
     // thread1.join();
      /*thread2.join();
      thread3.join();
      thread4.join();
      thread5.join();
      thread6.join();
      thread7.join();
      thread8.join();
      thread9.join();
      thread10.join();
      */
      //thread11.join();
      //thread12.join();
        
      //stmt.close();
      //conn.close();
    }
   catch(SQLException se){
      se.printStackTrace();
   }
   catch(Exception e){
      e.printStackTrace();
   }
   finally{
      try{
         if(stmt!=null)
            conn.close();
      }catch(SQLException se){
      }
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }
   }
}    
}
