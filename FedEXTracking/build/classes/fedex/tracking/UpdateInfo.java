/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fedex.tracking;

import static fedex.tracking.SqlManager.DB_URL;
import static fedex.tracking.SqlManager.PASS;
import static fedex.tracking.SqlManager.USER;
import static fedex.tracking.SqlManager.conn;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Karthik
 */
import java.sql.*;
public class UpdateInfo {
     public static void updateThis(int TrackingId, Double Distance) throws ClassNotFoundException, SQLException, InterruptedException
    {
        Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = con.createStatement();
        Connection c = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement s = c.createStatement();
        String query = "Select * from packages WHERE TrackingId = '"+ TrackingId +"'";
        ResultSet result = stmt.executeQuery(query);
        while(result.next())
            {
                String hist = new String();
                String resul = Dijkstra.getPath(result.getString("StoreHouse"), result.getString("Destination"), Distance);
                //System.out.println(result.getInt("TrackingId")+" "+result.getString("Status"));
                if(result.getString("Status").equals("Waiting"))
                    hist = resul;
                else
                    hist = result.getString("Status")+"\n"+resul;
                String query2 = "Update packages SET Status ='"+hist+"' where TrackingId = '"+ TrackingId+"'";
                s.executeUpdate(query2);
               }
        result.close();
        
     }

     public static void updateAll(Double Distance) throws SQLException, InterruptedException, ClassNotFoundException
     {
        //conn.close();
        Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = con.createStatement();
        String query = "Select * from packages";
        try (ResultSet result = stmt.executeQuery(query)) {
            while(result.next())
            {
                //System.out.println(result.getString("TrackingId"));
                int track = result.getInt("TrackingId");
                UpdateInfo.updateThis(track, Distance);
            }
            
        }
         
     }
}    
