/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fedex.tracking;
import static fedex.tracking.SqlManager.DB_URL;
import static fedex.tracking.SqlManager.PASS;
import static fedex.tracking.SqlManager.USER;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Karthik
 */
public class PackageThread extends Thread {
    
          
     public void run()
    {
        
        
      Package box = new Package();  
      for(int i = 1; i < 100; i++)
      {
          try{
      Connection connect = DriverManager.getConnection(DB_URL, USER, PASS);
      Statement statmt = connect.createStatement();
       
          Double wKg = box.weightRandomizer();
          Double wlb = box.lbsConverter();
          String startCity = box.startCityRandomizer();
          String destination = box.destinationRandomizer(startCity);
          SqlManager.insertInTable(connect, statmt, wKg, wlb, box.dimensionsRandomizer(),box.fetchTotalPieces(), startCity, destination);
          connect.close();
          statmt.close();
          } catch (SQLException | InterruptedException ex) {
              Logger.getLogger(PackageThread.class.getName()).log(Level.SEVERE, null, ex);
          }
      }
        
    }
}
