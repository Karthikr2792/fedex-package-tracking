package fedex.tracking;

public class Vertex implements Comparable<Vertex> {
    public final String name;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;    
    
    public void Vertex(){}
    public Vertex(String argName)
    { this.name = argName;}
    
    public String toString()
    { return this.name; }
    
    public int compareTo(Vertex other)
    {
        return Double.compare(minDistance, other.minDistance);
    }
    
}
