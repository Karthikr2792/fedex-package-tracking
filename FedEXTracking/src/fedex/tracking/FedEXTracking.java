/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fedex.tracking;


import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Karthik
 */
public class FedEXTracking {

    /**
     * @param args the command line arguments
     */  

    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        Statement stmt = null;
                
   try{
      SqlManager.establishConnection();
      conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");
      stmt = conn.createStatement();
      Package box = new Package();
     
      /*
       
      for(int i = 1; i < 100000;i++)
      {
          Double wKg = box.weightRandomizer();
          Double wlb = box.lbsConverter();
          String startCity = box.startCityRandomizer();
          String destination = box.destinationRandomizer(startCity);
          SqlManager.insertInTable(wKg, wlb, box.dimensionsRandomizer(),box.fetchTotalPieces(), startCity, destination);
                  
      }
     
    */
    UpdateThread thread11 = new UpdateThread();
      thread11.start();
      
    QueryThread thread12 = new QueryThread();
      thread12.start();
    
     
        
      //stmt.close();
      //conn.close();
    }
   catch(SQLException se){
      se.printStackTrace();
   }
   catch(Exception e){
      e.printStackTrace();
   }
   finally{
      try{
         if(stmt!=null)
            conn.close();
      }catch(SQLException se){
      }
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }
   }
}    
}
