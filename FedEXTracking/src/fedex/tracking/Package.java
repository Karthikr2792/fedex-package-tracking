/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fedex.tracking;

/**
 *
 * @author Karthik
 */
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Random;
import static java.lang.Math.random;
import java.util.ArrayList;
import java.util.List;

public class Package {

    /*
    This class contains a Tracking ID, Weight of package(randomize)
    (include kg to lb conversion), dimensions of package(randomize inches), 
    Service type(FedEx home delivery), total pieces(1 to 9), Packaging(package),
    Signature Services(Direct Signature Required), Special Handling Section
    (Direct Signature Required).    
    */
    private static SecureRandom ID = new SecureRandom();
    private String TrackingNumber;
    private double weightInKg, weightInLbs;
    private int[] dimensions;
    public String serviceType = "FedEx Home Delivery";//FedEx Home Delivery
    private int totalPieces; 
    public String packaging = "Package"; //Package
    public String signatureServices = "Direct Signature Required";//Direct Signature Requiered
    public String specialHandling = signatureServices;
    int dimensionMaxValue= 45;//used in dimensionRandomizer method
    private String Dimension_string;
    HashMap<String, Vertex> Cities = new HashMap<String, Vertex>();
    String[] CityNames = {"Northborough","Edison", "Pittsburgh", "Allentown","Martinsburg","Charlotte","Atlanta","Orlando","Memphis",
        "Grove City", "Indianapolis","Detroit","New Berlin","Minneapolis","St. Louis","Kansas","Dallas","Houston","Denver","Salt Lake City",
        "Phoenix","Los Angeles","Chino","Sacramento","Seattle"};
    public List<Vertex> path = new ArrayList<Vertex>();

    public Package() {
        
        this.dimensions = new int[3];        
    }
    
    public final String getTrackingID()
    {
        /*
        Tracking ID is an alphanumeric key saved in a string.
        Class contains Generation, extract a tracking ID.
        Param: None
        Return: String TrackingNumber
        */
        this.TrackingNumber = new BigInteger(50, ID).toString(36).toUpperCase();
        return this.TrackingNumber;
    }
    
    public double weightRandomizer()
    {
        /*
        Random weight generated. Weight limit from 0.5 to 20 
        Param: None
        Return: double weightinKg
        */
        Random generator = new Random();
        this.weightInKg = 0.5 + (20 - 0.5)*generator.nextDouble();
        DecimalFormat form = new DecimalFormat("##.00");
        this.weightInKg = Double.parseDouble(form.format(this.weightInKg));
        return this.weightInKg;        
    }
    public double lbsConverter()
    {
        this.weightInLbs = 2.21*this.weightInKg;
        DecimalFormat form = new DecimalFormat("##.00");
        this.weightInLbs = Double.parseDouble(form.format(this.weightInLbs));
        return this.weightInLbs;
    }
    
    public String dimensionsRandomizer()
    {        
        /*
        Random dimension values assigned to an array of integer.
        Dimensions limited to max value of 45
        Param: None
        Return: String dimensions
        */
        for(int i =0; i <=2; i++)
        {
            Random generator = new Random();
            int j = generator.nextInt() % dimensionMaxValue;
            this.dimensions[i] =  Math.abs(3 + j);
        }
       Dimension_string = "" + this.dimensions[0]+ "x" + this.dimensions[1] + "x"+ 
               this.dimensions[2] + "in";
       return this.Dimension_string;
    }
    
    public int fetchTotalPieces(){
        /*
        Random TotalPieces in package generated. Limit is 9 pieces per package
        Param: None
        Return: int totalPieces 
       */
        Random generator = new Random();
        int j = generator.nextInt() % 10;
        this.totalPieces = Math.abs(2+j);
        if(this.totalPieces == 0)
            this.totalPieces+=1;
        return this.totalPieces;
    }
    
   public HashMap addVertexToMap(Vertex city)
   {
       Cities.put(city.name, city);
       return this.Cities;
   }
   
   public String startCityRandomizer()
   {
       /*
       Returns city name for starting point.
       Param: None
       Return: String cityname
       */
       Random rnd = new Random();
       int index = rnd.nextInt(CityNames.length);
       String city = CityNames[index];
       return city;       
   }
   
   public String destinationRandomizer(String starting_node)
   {
       Random rnd = new Random();
       int index = rnd.nextInt(CityNames.length);
       String city = CityNames[index];
       if(city==starting_node)
           city=CityNames[(index+1)%CityNames.length];
       return city;
   }

    
   
   
    
}
 