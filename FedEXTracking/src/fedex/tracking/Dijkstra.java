package fedex.tracking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.PriorityQueue;

public class Dijkstra{
    //public String[] pathNodes = {};
    
    public static String update;
	public static void computePaths(Vertex source)
    {
        source.minDistance = 0;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
	vertexQueue.add(source);

	while (!vertexQueue.isEmpty()) {
	    Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.adjacencies)
            {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
		if (distanceThroughU < v.minDistance) {
		    vertexQueue.remove(v);

		    v.minDistance = distanceThroughU ;
		    v.previous = u;
                    vertexQueue.add(v);
                    //System.out.println(v);
		}
            }
        }
    }

    public static List<Vertex> getShortestPathTo(Vertex target)
    {
        List<Vertex> path = new ArrayList<Vertex>();
        
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            {
            path.add(vertex);
            Vertex endNode = vertex;
            
            /*if(vertex.previous == null)
            {
                System.out.println("Status: In Transit from " + endNode);
            }
            else
        	System.out.println("Status: In Transit from " + endNode + "\nTotal Distance Travelled: " + endNode.minDistance+"\n");
            try{
                Thread.sleep(3000);
            }
            catch(InterruptedException e){
            e.printStackTrace();
            }*/
            }
            
        Collections.reverse(path);
        return path;
    }

    /**
     *
     * @param startCity
     * @param destination
     */
    public static String getPath(String startCity, String destination, Double distance) throws InterruptedException
    {
    
    	Vertex losAngeles = new Vertex("Los Angeles"); Vertex sacramento = new Vertex("Sacramento"); Vertex chino = new Vertex("Chino");
    	Vertex saltLakeCity = new Vertex("Salt Lake City"); Vertex phoenix = new Vertex("Phoenix"); Vertex denver = new Vertex("Denver");
    	Vertex seattle = new Vertex("Seattle"); Vertex minneapolis = new Vertex("Minneapolis"); Vertex newBerlin = new Vertex("New Berlin");
    	Vertex indianapolis = new Vertex("Indianapolis"); Vertex kansas = new Vertex("Kansas"); Vertex dallas = new Vertex("Dallas");
    	Vertex houston = new Vertex("Houston"); Vertex memphis = new Vertex("Memphis"); Vertex stLouis = new Vertex("St. Louis");
    	Vertex atlanta = new Vertex("Atlanta"); Vertex orlando = new Vertex("Orlando"); Vertex charlotte = new Vertex("Charlotte");
    	Vertex detroit = new Vertex("Detroit"); Vertex groveCity = new Vertex("Grove City"); Vertex pittsburgh = new Vertex("Pittsburgh");
        Vertex martinsburg = new Vertex("Martinsburg"); Vertex northborough = new Vertex("Northborough"); Vertex allenTown = new Vertex("Allentown"); Vertex edison = new Vertex("Edison");
    	
    	Vertex[] cities = {losAngeles, sacramento, chino, seattle, minneapolis, newBerlin, indianapolis, kansas, dallas, houston, memphis, stLouis, 
            atlanta, orlando, charlotte, detroit, groveCity,pittsburgh, martinsburg, northborough, allenTown, edison};
    	
	losAngeles.adjacencies = new Edge[]{ new Edge(sacramento,  350),
	                             		 new Edge(chino,  50),
	                             		 new Edge(saltLakeCity, 800),
	                             		 new Edge(phoenix, 400),
	                             		 new Edge(denver, 1000),
	                             		 new Edge(seattle, 1000)};
	
	sacramento.adjacencies = new Edge[]{ new Edge(losAngeles,  350)};
	
	chino.adjacencies = new Edge[]{ new Edge(losAngeles, 50)};
	
	saltLakeCity.adjacencies = new Edge[]{ new Edge(losAngeles, 800),
	                             		   new Edge(denver, 500)};
	
	phoenix.adjacencies = new Edge[]{ new Edge(losAngeles, 400),
									  new Edge(denver, 850),
									  new Edge(dallas, 1000)};
	
	denver.adjacencies = new Edge[]{ new Edge(losAngeles,  1000),
	                             	 new Edge(saltLakeCity, 500),
	                             	 new Edge(seattle, 1200),
	                             	 new Edge(kansas, 600),
	                             	 new Edge(dallas, 800),
	                             	 new Edge(phoenix, 850)};
	
	seattle.adjacencies = new Edge[]{ new Edge(losAngeles, 1000),
	                             	  new Edge(denver, 1200),
	                             	  new Edge(kansas, 1300),
	                             	  new Edge(indianapolis, 1500),
	                             	  new Edge(minneapolis, 1300)};
	
	minneapolis.adjacencies = new Edge[]{ new Edge(seattle, 1300),
  		   								  new Edge(newBerlin, 300)};
	
	newBerlin.adjacencies = new Edge[]{ new Edge(minneapolis, 300),
  		   								new Edge(indianapolis, 300)};
	
	indianapolis.adjacencies = new Edge[]{ new Edge(newBerlin, 300),
       	  								   new Edge(seattle, 1500),
       	  								   new Edge(kansas, 500),
       	  								   new Edge(pittsburgh, 300),
       	  								   new Edge(groveCity, 150),
       	  								   new Edge(detroit, 300)};
	
	kansas.adjacencies = new Edge[]{ new Edge(seattle, 1300),
       	  							 new Edge(denver, 600),
       	  							 new Edge(dallas, 550),
       	  							 new Edge(stLouis, 250),
       	  							 new Edge(atlanta, 600),
       	  							 new Edge(indianapolis, 500)};
	
	dallas.adjacencies = new Edge[]{ new Edge(phoenix, 1000),
				 					 new Edge(houston, 200),
				 					 new Edge(memphis, 450),
				 					 new Edge(kansas, 550),
				 					 new Edge(atlanta, 800),
				 					 new Edge(denver, 800)};
	
	houston.adjacencies = new Edge[]{ new Edge(dallas, 200)};
	
	memphis.adjacencies = new Edge[]{ new Edge(atlanta, 400),
			  						  new Edge(stLouis, 300),
			  						  new Edge(dallas, 450)};
	
	stLouis.adjacencies = new Edge[]{ new Edge(kansas, 250),
			  						  new Edge(memphis, 300),
			  						  new Edge(atlanta, 400)};
	
	atlanta.adjacencies = new Edge[]{ new Edge(orlando,  450),
        	 						  new Edge(charlotte, 250),
        	 						  new Edge(pittsburgh, 650),
        	 					 	  new Edge(kansas, 600),
        	 						  new Edge(dallas, 800),
        	 						  new Edge(stLouis, 400),
        	 						  new Edge(memphis, 400)};
	
	orlando.adjacencies = new Edge[]{ new Edge(atlanta, 450)};
	
	charlotte.adjacencies = new Edge[]{ new Edge(atlanta, 250), new Edge(martinsburg, 380)};
	
	groveCity.adjacencies = new Edge[]{ new Edge(detroit, 200),new Edge(indianapolis, 150),new Edge(pittsburgh, 200)};
	
	detroit.adjacencies = new Edge[]{ new Edge(indianapolis, 300), new Edge(groveCity, 200)};
	
	pittsburgh.adjacencies = new Edge[]{ new Edge(groveCity, 200),new Edge(indianapolis, 300),new Edge(atlanta, 650),
            new Edge(edison, 250),new Edge(allenTown, 250),new Edge(northborough, 400), new Edge(martinsburg, 150)};
	
	martinsburg.adjacencies = new Edge[]{ new Edge(pittsburgh, 150), new Edge(charlotte, 380)};
	
	northborough.adjacencies = new Edge[]{ new Edge(pittsburgh, 400),
            new Edge(edison, 200),new Edge(allenTown, 200)};
	
	edison.adjacencies = new Edge[]{ new Edge(pittsburgh, 250),
            new Edge(northborough, 200),new Edge(allenTown, 50)};
	
	allenTown.adjacencies = new Edge[]{ new Edge(pittsburgh, 250),
            new Edge(edison, 50),new Edge(northborough, 200)};
	
       String dbStartName = startCity;
       Vertex startNode = allenTown;
       String dbEndName = destination;
       Vertex endNode = seattle;
       for (Vertex v : cities){
           	if(v.name.equals(dbStartName))
                {
                   startNode = v;
                }
       }computePaths(startNode);
       
       for(Vertex v : cities){             
                if(v.name.equals(dbEndName))
                {   
                    endNode = v;
                }
       }
       List<Vertex> path = getShortestPathTo(endNode);
        
       for(int i = 0; i < path.size(); i++)
       {
           if(distance==0)
           {
               String update = "In FedEx possession at "+ path.get(i);
               //System.out.println(update);
               return update;
               
           }
           else if((i == path.size()-1) &&(distance >= path.get(i).minDistance))
           {
               String update = "Delivered successfully at "+ path.get(i);
               //System.out.println(update);
               return update;
               
           }
           
           else if(distance >= path.get(i).minDistance && distance <= path.get(i+1).minDistance)
           {
               String update = "Arrived at "+ path.get(i) + ". In Transit to "+ path.get(i+1) +".";
               //System.out.println(update);
               return update;
           }
           
       }
       
       return update;
    }
    
	}
    
