/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fedex.tracking;

/**
 *
 * @author Karthik
 */
import static fedex.tracking.SqlManager.DB_URL;
import static fedex.tracking.SqlManager.PASS;
import static fedex.tracking.SqlManager.USER;
import static fedex.tracking.SqlManager.conn;
import java.util.Scanner;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryThread extends Thread {
	public static String TrackingID;
     
        @Override
	public void run(){
            
            
                while(true){
                Scanner in = new Scanner(System.in);
                System.out.println("Enter Tracking ID:");
                TrackingID = in.nextLine();
                try {
                    Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
                        Statement s = conn.createStatement();
                        SqlManager.retrieveFromTable(s, conn, TrackingID);
                        s.close();
                        conn.close();
                        
                    } 
                catch (SQLException | InterruptedException ex) {
                        Logger.getLogger(QueryThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
        }

}
